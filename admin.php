<?php
include("config.php");
include("utils.php");
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Pendu App: Interface d’admin</title>
  <link rel="stylesheet" media="screen" href="style.css">
</head>
<body>
  <div id="mots">
    <ul>
      <?php
      $query = "SELECT * FROM mots";
      $result = mysqli_query($handle,$query);
      while($line = mysqli_fetch_array($result)) {
        echo "\t\t<li>";
        echo $line["mot"];
        echo "&nbsp;<a href=\"delete_mot.php?id=" . $line["id"] . "\">X</a>";
        echo "&nbsp;<a href=\"update_mot.php?id=" . $line["id"] . "\">U</a>";
        echo "</li>\n";
      }
      ?>
    </ul>
    <form method="post" action="create_mot.php">
      <label for="mot">Nouveau mot:&nbsp;</label>
      <input name="mot" type="text" placeholder="nouveau mot">
      <input type="submit">
    </form>
  </div>
  <div id="joueurs">
    <ul>
      <?php
      $query = "SELECT * FROM joueurs";
      $result = mysqli_query($handle,$query);
      while($line = mysqli_fetch_array($result)) {
        echo "\t\t<li>";
        echo $line["noms"];
        echo "&nbsp;<a href=\"delete_joueur.php?id=" . $line["id"] . "\">X</a>";
        echo "&nbsp;<a href=\"update_joueur.php?id=" . $line["id"] . "\">U</a>";
        echo "</li>\n";
      }
      ?>
    </ul>
    <form method="post" action="create_joueur.php">
      <label for="joueur">Nouveau joueur:&nbsp;</label>
      <input name="joueur" type="text" placeholder="nouveau joueur">
      <input type="submit">
    </form>
  </div>
</body>
</html>
